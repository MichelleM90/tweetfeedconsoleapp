# Tweet Feed Console Application

Program to simulate a simple twitter-like feed.
It takes 2 filenames as inputs (these files must be in the same folder as the exe)
These files must be in the order UserFile first and then Tweet file.
The file format for the user must be as follows:  
Ward follows Alan  
Alan follows Martin  
Ward follows Martin, Alan  
The file format for the tweet file must be as follows:  
Alan> If you have a procedure with 10 parameters, you probably missed some.  
Ward> There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.  
Alan> Random numbers should not be generated with a method chosen at random.  

It then displays the following output in the console for the above inputs:  
Alan  
&nbsp;&nbsp;&nbsp;@Alan: If you have a procedure with 10 parameters, you probably missed some.  
&nbsp;&nbsp;&nbsp;@Alan: Random numbers should not be generated with a method chosen at random.  
Martin  
Ward  
&nbsp;&nbsp;&nbsp;@Alan: If you have a procedure with 10 parameters, you probably missed some.  
&nbsp;&nbsp;&nbsp;@Ward: There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.  
&nbsp;&nbsp;&nbsp;@Alan: Random numbers should not be generated with a method chosen at random.  

## Getting Started

To get the project files, go to https://gitlab.com/MichelleM90/tweetfeedconsoleapp/tree/master.
From here download the source code as a zip file.
Extract this zip file.
Open the solution file (TweetFeedConsoleApp.sln) to view the code.
You can run the unit tests in the TweetFeedConsoleApp.Test project.

### Prerequisites

To view the code you require some application such as Visual Studio.
This project was coded in C# in Visual Studio Community 2017.

### Installing

To run the code open the solution file in Visual Studio and build it in release mode or debug mode.
(may need to rename the extracted zip file name)

## Running the tests

After the solution is built, you should have all the tests that you can run in the test explorer

## Deployment

There are example files that you can use in the example folder as input.
Insert the example files user.txt and tweet.txt into the same folder as the exe application

Open cmd.exe and run the exe while providing the 2 filenames:
e.g. TweetFeedConsoleApp.exe "user.txt" "tweet.txt"

Can also run from Visual Studio by editting the project build properties.

## Authors

* **Michelle Mohabir** - *Initial work*

## License

None