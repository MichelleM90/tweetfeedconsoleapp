﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace TweetFeedConsoleApp.Test
{
    [TestClass()]
    public class ProgramTests
    {

        [TestMethod()]
        public void TwoFileNamesProvidedByUserTest_WithNoFilesProvided() {
            string userFileName = "";
            string tweetFileName = "";
            string[] args = { "" };
            bool result = Program.TwoFileNamesProvidedByUser(args, ref userFileName, ref tweetFileName);
            if (result) {
                Assert.Fail();
            }
            if (userFileName.Length > 0 || tweetFileName.Length > 0) {
                Assert.Fail();
            }
        }

        [TestMethod()]
        public void TwoFileNamesProvidedByUserTest_WithOneFileProvided() {
            string userFileName = "";
            string tweetFileName = "";
            string[] args = { "someFileName.txt" };
            bool result = Program.TwoFileNamesProvidedByUser(args, ref userFileName, ref tweetFileName);
            if (result) {
                Assert.Fail();
            }
            if (userFileName.Length > 0 || tweetFileName.Length > 0) {
                Assert.Fail();
            }
        }

        [TestMethod()]
        public void TwoFileNamesProvidedByUserTest_WithBothFileNamesProvided() {
            string userFileName = "";
            string tweetFileName = "";
            string[] args = { "someFileName1.txt", "someFileName2.txt" };
            bool result = Program.TwoFileNamesProvidedByUser(args, ref userFileName, ref tweetFileName);
            if (!result) {
                Assert.Fail();
            }
            if (!(userFileName.Equals(args[0])) || !(tweetFileName.Equals(args[1]))) {
                Assert.Fail();
            }
        }

        [TestMethod()]
        public void FileExistsInCurrentDirectoryTest_WithFileThat_Exists() {
            string filename = "TweetFeedConsoleApp.exe";
            bool result = Program.FileExistsInCurrentDirectory(filename);
            if (!result) {
                Assert.Fail();
            }
        }

        [TestMethod()]
        public void FileExistsInCurrentDirectoryTest_WithFileThat_DoesNotExist() {
            string filename = "NotHere.txt";
            bool result = Program.FileExistsInCurrentDirectory(filename);
            if (result) {
                Assert.Fail();
            }
        }

        [TestMethod()]
        public void GetDetailsOfAllUsersTest_WithEmptyFile() {
            string userFile = "";
            List<UserDetail> result = Program.GetDetailsOfAllUsers(userFile);
            if (result.Count > 0) {
                Assert.Fail();
            }
        }

        [TestMethod()]
        public void GetUserFromLineTest_WithValidFormat() {
            string userLineText = "Ward follows Alan";
            UserDetail expectedResult = new UserDetail();
            expectedResult.UserName = "Ward";
            expectedResult.UserFollows = new List<string> { "Alan" };
            UserDetail result = Program.GetUserFromLine(userLineText);
            if ((result.UserName != expectedResult.UserName) || !(result.UserFollows.All(expectedResult.UserFollows.Contains))
                || (result.UserFollows.Count != expectedResult.UserFollows.Count)) {
                Assert.Fail();
            }
        }

        [TestMethod()]
        public void GetUserFromLineTest_WithInValidFormat() {
            string userLineText = "Ward followses Alan";
            UserDetail result = Program.GetUserFromLine(userLineText);
            if (result.UserName != null) {
                Assert.Fail();
            }
        }

        [TestMethod()]
        public void AddAllUsersThatAreFollowedToListTest_With2UserNames_ThatDoNotFollowAnyone() {

            UserDetail userInput1 = new UserDetail()
            {
                UserName = "Alan",
                UserFollows = "Martinon,Freddy,Frank".Split(',').ToList()
            };
            UserDetail userInput2 = new UserDetail()
            {
                UserName = "Martinon",
                UserFollows = "Alan,Freddy".Split(',').ToList()
            };
            List<UserDetail> userList = new List<UserDetail>();
            userList.Add(userInput1);
            userList.Add(userInput2);

            Program.AddAllUsersThatAreFollowedToList(userList);

            if (userList.Count != 4) { // could do a more comprehensive test
                Assert.Fail();
            }
        }

        [TestMethod()]
        public void GetTweetFromLineTest_WithCorrectFormat_AndUserThatExists() {
            string tweetLine = "Alan> I'm not sure what I'm doing";
            UserDetail userInput1 = new UserDetail()
            {
                UserName = "Alan",
                UserFollows = "Martinon,Freddy,Frank".Split(',').ToList()
            };
            List<UserDetail> userList = new List<UserDetail>();
            userList.Add(userInput1);
            Tweet tweet = Program.GetTweetFromLine(tweetLine, userList);
            if ((tweet.TweetMessage != "I'm not sure what I'm doing") || (tweet.User.UserName != "Alan")) {
                Assert.Fail();
            }
        }

        [TestMethod()]
        public void GetTweetFromLineTest_WithCorrectFormat_AndUserThatDoesntExists() {
            string tweetLine = "Alan> I'm not sure what I'm doing";
            UserDetail userInput1 = new UserDetail()
            {
                UserName = "Ward",
                UserFollows = "Martinon,Freddy,Frank".Split(',').ToList()
            };
            List<UserDetail> userList = new List<UserDetail>();
            userList.Add(userInput1);
            Tweet tweet = Program.GetTweetFromLine(tweetLine, userList);
            if ((tweet.TweetMessage != null) & (tweet.User != null)) {
                Assert.Fail();
            }
        }

        [TestMethod()]
        public void GetTweetFromLineTest_WithIncorrectFormat() {
            string tweetLine = "Alan I'm not sure what I'm doing";
            UserDetail userInput1 = new UserDetail()
            {
                UserName = "Alan",
                UserFollows = "Martinon,Freddy,Frank".Split(',').ToList()
            };
            List<UserDetail> userList = new List<UserDetail>();
            userList.Add(userInput1);
            Tweet tweet = Program.GetTweetFromLine(tweetLine, userList);
            if ((tweet.TweetMessage != null) & (tweet.User != null)) {
                Assert.Fail();
            }
        }
    }
}
