﻿/*  Author:             Michelle Mohabir
 *  Date Created:       2017-04-08
 *  Date Modified:      2017-04-12
 *  Description:        Console Application that takes 2 inputs (the file name of a user txt file and tweet text file)
 *                      It processes these 2 files to give the user a nice display of all the users and the tweets of the users and the users they follow                            
 */

using System;
using System.Collections.Generic;

namespace TweetFeedConsoleApp
{
    public class UserDetail : IComparable
    {
        string userName;
        List<string> userFollows;

        public string UserName
        {
            get
            {
                return userName;
            }

            set
            {
                userName = value;
            }
        }

        public List<string> UserFollows
        {
            get
            {
                return userFollows;
            }

            set
            {
                userFollows = value;
            }
        }

        int IComparable.CompareTo(object obj) { //adds ability to sort the List<UserDetail> by userName
            UserDetail u = (UserDetail)obj;  
            return String.Compare(UserName, u.UserName);
        }
    }
}
