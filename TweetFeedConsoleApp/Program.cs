﻿/*  Author:             Michelle Mohabir
 *  Date Created:       2017-04-08
 *  Date Modified:      2017-04-12
 *  Description:        Console Application that takes 2 inputs (the file name of a user txt file and tweet text file)
 *                      It processes these 2 files to give the user a nice display of all the users and the tweets of the users and the users they follow                            
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TweetFeedConsoleApp
{
    public class Program
    {
        static void Main(string[] args) {

            try {
                string userFileName = "";
                string tweetFileName = "";

                if (TwoFileNamesProvidedByUser(args, ref userFileName, ref tweetFileName)) { //It is assumed that the filenames are provided in the correct order (user filename and tweet filename)

                    #region check file existence
                    if (FileExistsInCurrentDirectory(userFileName) != true) { //checks that the currect working directory contains the provided filename. Prints a message to console if no file found
                        Console.ReadLine();
                        return; //ends program if file not found 
                    }
                    if (FileExistsInCurrentDirectory(tweetFileName) != true) {
                        Console.ReadLine();
                        return;
                    }
                    #endregion

                    List<UserDetail> userList = GetDetailsOfAllUsers(userFileName);
                    List<Tweet> tweetList = GetAllTweets(tweetFileName, userList);

                    Display(userList, tweetList);
                }
            }
            catch (Exception e) {
                Console.WriteLine("A fatal error occured " + e.StackTrace);
            }
            finally {
                Console.ReadLine();
            }


        }
        #region FileValidationMethods
        /// <summary>
        /// Checks that 2 strings are given by the user on running the application.
        /// If it finds 2 strings then gets the filename from these strings and stores them in the userFileName and tweetFileName variable.
        /// Prints a message on the Console if 2 strings are not given.
        /// </summary>
        /// <param name="args"></param>
        /// <param name="userFileName"></param>
        /// <param name="tweetFileName"></param>
        /// <returns></returns>
        public static bool TwoFileNamesProvidedByUser(string[] args, ref string userFileName, ref string tweetFileName) {
            if (args == null) {
                Console.WriteLine("No file names given"); // Check for null array
            }
            else {
                if (args.Length == 2) {
                    userFileName = args[0];
                    tweetFileName = args[1];
                    return true;
                }
                else {
                    Console.WriteLine("Two fileNames must be given");
                }
            }
            return false;
        }

        /// <summary>
        /// Checks that the filename exists in the current working directory.
        /// Note that using the working directory is not recommended.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns>true if found and false if not found</returns>
        public static bool FileExistsInCurrentDirectory(string fileName) {
            string currentDirectory = Directory.GetCurrentDirectory();
            if (File.Exists(currentDirectory + "\\" + fileName)) { //file found
                return true;
            }
            else {
                Console.WriteLine(string.Format("\"{0}\" file not found in {1}", fileName, currentDirectory)); //file not found
                return false;
            }
        }
        #endregion

        #region User file processing methods
        /// <summary>
        /// Processes the user text file as follows:
        /// Adds a user to the list if it doesn't exist in the list already. 
        /// If the user exists, it then merges the users that the user follows into a single list without duplicates.
        /// It then checks that all the users being followed are added to the list if they are not there.
        /// The list is sorted.
        /// </summary>
        /// <param name="userFileName"></param>
        /// <returns>A sorted list of users is returned</returns>
        public static List<UserDetail> GetDetailsOfAllUsers(string userFileName) {
            List<UserDetail> userList = new List<UserDetail>();

            if (userFileName.Length > 0) {
                using (StreamReader file = new StreamReader(userFileName)) {
                    // Get the user detail per line of the user file
                    int counter = 0;
                    string line;
                    while ((line = file.ReadLine()) != null) {
                        UserDetail user = GetUserFromLine(line); //
                        if (user.UserName != null) {
                            var userExists = userList.FindIndex(c => c.UserName == user.UserName); //check for duplicate user
                            if (userExists >= 0) //if duplicate user update the users they follow
                            {
                                userList[userExists] = new UserDetail { UserName = user.UserName, UserFollows = userList[userExists].UserFollows.Union(user.UserFollows).ToList() }; //Updates list of users that a user follows
                            }
                            else {
                                userList.Add(user);
                            }
                        }
                        counter++;
                    }
                }
                AddAllUsersThatAreFollowedToList(userList);//get the users that are only being followed
                userList.Sort();
            }
            return userList;
        }

        /// <summary>
        /// Processes a single line of the user file to return a user and list of users they follow.
        /// Prints a warning if the keyword " follows " is not found
        /// Assumption that the user name doesn't contain spaces and isn't " follows "
        /// </summary>
        /// <param name="line"></param>
        /// <returns>A single user</returns>
        public static UserDetail GetUserFromLine(string line) {
            UserDetail userData = new UserDetail();
            int charLocation = line.IndexOf(" follows ", StringComparison.OrdinalIgnoreCase); //gets the location of the word " follows "

            if (charLocation > 0) {
                userData.UserName = line.Substring(0, charLocation); //gets the name of the user that follows other users
                userData.UserFollows = line.Substring(charLocation + " follows ".Length, line.Length - (charLocation + " follows ".Length))
                    .Split(',').Select(UserFollows => UserFollows.Trim()).ToList(); //gets the string after the special word " follows ". Inserts this into a list using the comma as a delimiter and removes the empty spaces from the names.
            }
            else {
                Console.WriteLine("User file is not in the correct format, please ensure that the exact word ' follows ' is used");
            }
            return userData;
        }

        /// <summary>
        /// Adds all users that are followed but are not following anyone to the user list.
        /// </summary>
        /// <param name="userList"></param>
        public static void AddAllUsersThatAreFollowedToList(List<UserDetail> userList) {
            foreach (var user in userList.ToList()) {
                foreach (var userFollower in user.UserFollows) { //loop through all 
                    var userExists = userList.FindIndex(c => c.UserName == userFollower); //check if follower exists as a user already and ignore if they exist
                    if (userExists == -1) { //if user does not exist
                        userList.Add(new UserDetail { UserName = userFollower, }); //add user that does not follow any users
                    }
                }
            }
        }
        #endregion

        #region Tweet file processing methods
        /// <summary>
        /// Processes the tweet file as follows:
        /// Reads each line from the tweet file and sends it to GetTweetFromLine method to check if it has an associated user and limits its length 
        /// </summary>
        /// <param name="tweetFileName"></param>
        /// <param name="userList"></param>
        /// <returns>A list of tweets</returns>
        public static List<Tweet> GetAllTweets(string tweetFileName, List<UserDetail> userList) {
            List<Tweet> tweetList = new List<Tweet>();
            // Get the tweet detail per line of the tweet file
            using (StreamReader file = new StreamReader(tweetFileName)) {
                int counter = 0;
                string line;
                while ((line = file.ReadLine()) != null) {
                    Tweet message = GetTweetFromLine(line, userList);
                    if (message.TweetMessage != null) {
                        tweetList.Add(message);
                    }
                    counter++;
                }
            }
            return tweetList;
        }

        /// <summary>
        /// Gets the tweeters username and checks that they are in the User list.
        /// It then gets the tweet data with the length limited to 140 and the username
        /// </summary>
        /// <param name="line"></param>
        /// <param name="userList"></param>
        /// <returns>A single tweet</returns>
        public static Tweet GetTweetFromLine(string line, List<UserDetail> userList) {
            Tweet tweetData = new Tweet();
            int charLocation = line.IndexOf("> ", StringComparison.Ordinal); //gets the location of the "> " keyword. Assumption that user doesn't use this in their name
            string nameFound = "";
            int userIndexFound = -1;
            string fullTweetMessage = "";
            if (charLocation > 0) {
                nameFound = line.Substring(0, charLocation); //gets the name of the tweeter for the line of text
                userIndexFound = userList.FindIndex(u => u.UserName == nameFound); //assumption that names are case sensitive
                if (userIndexFound > -1) {
                    //get the message
                    fullTweetMessage = line.Substring(charLocation + "> ".Length, line.Length - (charLocation + "> ".Length));
                    tweetData.TweetMessage = fullTweetMessage.Substring(0, Math.Min(fullTweetMessage.Length, 140));
                    tweetData.User = userList[userIndexFound];
                }
            }
            else {
                Console.WriteLine("Tweet file is not in the correct format, please ensure that the exact word '> ' is used");
            }
            return tweetData;
        }
        #endregion

        #region Display method
        /// <summary>
        /// Printes out each user and the tweets that they and the users they follow made in the following format:
        /// UserName 
        ///     @TweetUserName: TweetMessage   
        /// </summary>
        /// <param name="userList"></param>
        /// <param name="tweetList"></param>
        public static void Display(List<UserDetail> userList, List<Tweet> tweetList) {
            string message = "";
            foreach (UserDetail user in userList) {
                Console.WriteLine(user.UserName);
                if (user.UserFollows != null) {
                    foreach (Tweet tweet in tweetList.Where(t => user.UserFollows.Contains(t.User.UserName) || t.User.UserName == user.UserName)) { //gets all the tweets that were made by the user and the users they follow
                        message = "\t@" + tweet.User.UserName + ": " + tweet.TweetMessage;
                        Console.WriteLine(message);
                    }
                }
            }
        }
        #endregion
    }
}
