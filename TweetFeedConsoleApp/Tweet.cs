﻿/*  Author:             Michelle Mohabir
 *  Date Created:       2017-04-08
 *  Date Modified:      2017-04-12
 *  Description:        Console Application that takes 2 inputs (the file name of a user txt file and tweet text file)
 *                      It processes these 2 files to give the user a nice display of all the users and the tweets of the users and the users they follow                            
 */


namespace TweetFeedConsoleApp
{
    public class Tweet
    {
        string tweetMessage;
        UserDetail user;

        public string TweetMessage
        {
            get
            {
                return tweetMessage;
            }

            set
            {
                tweetMessage = value;
            }
        }

        public UserDetail User
        {
            get
            {
                return user;
            }

            set
            {
                user = value;
            }
        }
    }
}
